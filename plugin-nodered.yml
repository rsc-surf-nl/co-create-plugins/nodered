---
- name: Playbook to Install Node-RED
  gather_facts: false
  hosts: 
    - localhost
  become: true

  tasks:
    - name: Wait for system to become reachable
      wait_for_connection:
        timeout: 300

    - name: Gather facts for first time
      setup:

    - name: Ubuntu
      when: ansible_facts['os_family'] != 'Debian'
      fail:
        msg: The Node-RED component is only implemented for distros in the Debian family

    - name: Install required packages
      apt:
        name: curl
        state: present

    - name: Download and run the setup script for NodeSource binary distributions
      shell: curl -fsSL https://deb.nodesource.com/setup_current.x| bash -

    - name: Install latest version of Node.js
      apt:
        name: nodejs
        state: present

    - name: Install Node-RED with npm
      npm:
        name: node-red
        global: yes
        state: latest
        unsafe_perm: yes

    - name: Install node-red-admin command line tool
      npm:
        name: node-red-admin
        global: yes
        state: latest
        unsafe_perm: yes

    - name: Create Node-RED user directory
      # By default, this would be $HOME/.node-red
      # We want to use a common location for all users.
      file:
        path: /var/lib/node-red
        state: directory
        mode: '0755'

    - name: Create Node-RED settings.js
      copy:
        dest: /var/lib/node-red/settings.js
        content: |
            module.exports = {
                httpRoot: "{{ nodered_nginx_location|default('/') }}"
            }
        mode: '0644'

    - name: Create nginx location block
      copy:
        dest: /etc/nginx/app-location-conf.d/nodered.conf
        mode: 0644
        content: |
            location {{ nodered_nginx_location|default('/') }} {
                error_page 401 = @custom_401;
                auth_request /validate;
                auth_request_set $username $upstream_http_username;
                proxy_set_header REMOTE_USER $username;
                proxy_pass http://127.0.0.1:1880;
                proxy_redirect http://localhost:1880/ $scheme://$host/;
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection $connection_upgrade;
                client_max_body_size 10G;
            }

    - name: Check nginx configuration syntax
      command: nginx -t
      register: nginx_test
      ignore_errors: yes

    - name: Fail if nginx config test failed
      fail:
        msg: "Nginx configuration test failed"
      when: nginx_test.rc != 0

    - name: Restart nginx
      service:
        name: nginx
        state: restarted

    - name: Ensure systemd directory is created
      file:
        path: /usr/lib/systemd/system
        state: directory     

    - name: Create a systemd service file for Node-RED
      copy:
        dest: /usr/lib/systemd/system/node-red.service
        mode: 0644
        content: |
          [Unit]
          Description=Node-RED
          After=network.target

          [Service]
          Type=simple
          Environment="NODE_OPTIONS="
          Environment="NODE_RED_OPTIONS=-v"
          ExecStart=/usr/bin/node-red $NODE_OPTIONS $NODE_RED_OPTIONS --userDir /var/lib/node-red
          Nice=10
          SyslogIdentifier=Node-RED
          StandardOutput=syslog
          Restart=on-failure
          RestartSec=42s

          [Install]
          WantedBy=multi-user.target

    - name: Reload systemd
      systemd:
        daemon_reload: yes

    - name: Enable Node-RED service to start on boot
      systemd:
        name: node-red
        enabled: yes

    - name: Start Node-RED service
      systemd:
        name: node-red
        state: started
