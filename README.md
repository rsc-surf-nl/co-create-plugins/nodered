## Node-RED

## Description
This NodeRed Ansible playbook is designed to automate the installation and configuration of **Node-RED** component on any Debian/Ubuntu system. The playbook performs the following tasks:
- updating system packages, installing dependencies, 
- setting up Node.js, 
- customize Nginx reverse-proxy configuration https://servicedesk.surf.nl/wiki/display/WIKI/Catalog+item+with+SRAM+login 
- configure Node-RED to run as a systemd service and start on-boot

It is recommended to use the following Node-Red node https://flows.nodered.org/node/@digitalnodecom/node-red-contrib-generic-s3 for a pipelines which also write mesages to Object Store. All alternatives were tested and this is the only one which works as expected, configured with the specific ObjectStore(S3) credentials.


## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
## Key Tasks and Why They Are Necessary:

- Task `wait_for_connection`: Ensures that the system is ready to be managed over SSH
- Task Check OS Ubuntu, self-explanatory. 
- Install Node.js and dependencies: Node.js is the runtime for Node-RED.  
- Install NodeRED and Ensure Node-RED Configuration Directory Exists
- Set Node-RED baseURL in application settings.js
- Create Nginx includes directory, if it doesn't exist
- Add the NodeRed location block to Nginx Configuration for secure access to Node-RED and via Nginx reverse proxy according with creating a SRC Component doc: (https://servicedesk.surf.nl/wiki/display/WIKI/Catalog+item+with+SRAM+login) and restart Nginx.
	  - In Nginx, there are specific contexts for directives, and `server` blocks can only be used at the top level of an Nginx configuration file, in the main `nginx.conf` or a file that's directly included from it 
	  - Files under `/etc/nginx/conf.d/` or directories like `/etc/nginx/app-location-conf.d/` are usually location or server-level configuration snippets, only `location {}` or other directives, not full `server {}` blocks.
   
- Create and Configure systemd Service for Node-Red: make sure that Node-RED is managed as a system service and enable it to run on boot.
- Start and Enable Node-RED as a Service at system boot


## Usage

This ansible script is used by Surf Research Cloud Component: NodeRed through their Workspace creating workflow when selecting the Application Nojus CatalogItem.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status

